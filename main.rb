# coding: utf-8

require 'togglv8'
require 'json'
require 'colorize'

module TogglCounter
  extend self
  OPEN_SOURCE_TIMES = 2 # open source hours multiplier
  WORKING_HOURS_PER_DAY = 7
  HELPOUT_TIMES = 1.5

  attr_reader :api_token, :from_date, :to_date, :user, :summary, :holidays_count, :total_toggl_hours, :working_days, :paid_holidays

  def call
    get_api_token
    set_dates
    count_holidays
    count_toggl_hours
    count_paid_days
    count_working_days
    show_summary_statistics
  rescue Exception => e
    puts e.message.red
  end

  private

  def holidays
    [
      { text: '1 stycznia  (niedziela)	Nowy Rok, Świętej Bożej Rodzicielki', date: '01-01-2017' },
      { text: '6 stycznia  (piątek)	Trzech Króli (Objawienie Pańskie)', date: '06-01-2017' },
      { text: '16 kwietnia  (niedziela)	Wielkanoc', date: '16-04-2017' },
      { text: '17 kwietnia  (poniedziałek)	Poniedziałek Wielkanocny', date: '17-04-2017' },
      { text: '1 maja  (poniedziałek)	Święto Pracy', date: '01-05-2017' },
      { text: '3 maja  (Środa)	Święto Konstytucji 3 Maja', date: '03-03-2017' },
      { text: '4 czerwca  (niedziela)	Zesłanie Ducha Świętego (Zielone Świątki)', date: '04-06-2017' },
      { text: '15 czerwca  (czwartek)	Boże Ciało', date: '15-06-2017' },
      { text: '15 sierpnia  (wtorek)	Święto Wojska Polskiego, Wniebowzięcie Najświętszej Maryi Panny', date: '15-08-2017' },
      { text: '1 listopada  (Środa)	Wszystkich Świętych', date: '01-11-2017' },
      { text: '11 listopada  (sobota)	Święto Niepodległości', date: '11-11-2017' },
      { text: '25 grudnia  (poniedziałek)	Boże Narodzenie (pierwszy dzień)', date: '25-12-2017' },
      { text: '26 grudnia  (wtorek)	Boże Narodzenie (drugi dzień)', date: '26-12-2017' }
    ]
  end

  def count_holidays
    puts 'Counting holidays'
    @holidays_count = 0
    holidays.each do |holiday|
      d = Date.parse(holiday[:date])
      next if from_date > d
      break if to_date < d
      if d.saturday? || d.sunday?
        puts holiday[:text].red
      else
        puts holiday[:text].green
        @holidays_count += 1
      end
    end

    puts "Found #{holidays_count} days\n\r"
  end

  def count_toggl_hours
    puts 'Generating report. Please wait...'
    #  ap summary

    calculations = summary.each_with_object({}) do |project, object|
      project_id = project['id']
      client = project['title']['client']
      minutes = 0
      project['items'].each do |i|
        minutes += (i['title']['time_entry'] =~ /(helpout|help out)/i && !(client =~ /spark/i)) ? i['time'] * HELPOUT_TIMES : i['time']
      end
      sum_minutes = (minutes / 1000 / 60).round
      doubles_minutes = client =~ /spark/i ? sum_minutes * OPEN_SOURCE_TIMES : sum_minutes

      object[project_id] = {
        name: project['title']['project'],
        client: client,
        sum_minutes: sum_minutes,
        with_extra_minutes: doubles_minutes
      }
    end

    total_minutes = 0

    calculations.each do |id, project|
      puts "#{project[:name] || 'No project'} - Hours: #{to_hours(project[:sum_minutes])}h, hours with extras:  #{to_hours(project[:with_extra_minutes])}h"
      total_minutes += project[:with_extra_minutes]
    end

    @total_toggl_hours = to_hours(total_minutes)
  end

  def to_hours(minutes)
    (minutes / 60.0).round
  end

  def days_to_working_hours(days = 0)
    WORKING_HOURS_PER_DAY * days
  end

  def summary_params
    {
      user_ids: user['id'],
      since: from_date.iso8601,
      until: to_date.iso8601,
      rounding: 'off',
      calculate: 'time'
    }
  end

  def summary
    reports               = TogglV8::ReportsV2.new(api_token: api_token)
    reports.workspace_id  = @workspace_id
    reports.summary('', summary_params)
  end

  def count_working_days
    @working_days = 0
    (from_date...to_date).each { |d| @working_days += 1 if (1..5).include?(d.wday) }
  end

  def get_api_token
    print "Please enter Toggle API token: "
    @api_token   = gets.chomp
    toggl_api    = TogglV8::API.new(api_token)
    @user        = toggl_api.me(all=true)
    workspaces   = toggl_api.my_workspaces(user)
    @workspace_id = workspaces.select { |w| w['name'] =~ /spark/i  }.first['id']

    puts "Report for: #{user['fullname']} - #{user['email']}"
  rescue
    puts 'Wrong API token provided. Try again!'
    retry
  end

  def show_summary_statistics
    puts "\n\r" + '*' * 80
    puts "Total toggl hours worked: #{total_toggl_hours}".green
    puts "Total paid holidays hours: #{days_to_working_hours(paid_holidays)}".green

    puts "Total working days since start date: #{working_days} days, hours: #{days_to_working_hours(working_days)}".red

    puts "Together holidays: #{holidays_count} days or working hours: #{days_to_working_hours(holidays_count)}\n\r".red

    puts "You've got: #{total_toggl_hours - days_to_working_hours(working_days - holidays_count - paid_holidays)} extra hours"
  end

  def set_dates
    begin
      print "Please enter start date (DD-MM-YYYY), default '01-01-2017': "
      input_from_date = (input = gets.chomp).empty? ? '01-01-2017' : input
      @from_date = Date.parse(input_from_date)
      raise 'Wrong from date provided. Try again!' unless @from_date.between?(Date.new(2016,1,1), DateTime.now)
    rescue => e
      puts e.message.red
      retry
    end

    print_date(from_date)

    begin
      print "Please enter end date (DD-MM-YYYY), default '#{Time.now.strftime('%d-%m-%Y')}': "
      input_end_date = (input = gets.chomp).empty? ? Time.now.strftime('%d-%m-%Y') : input
      @to_date = Date.parse(input_end_date)
      raise 'Wrong end date provided. Max range from start date is 1 year. Try again!' unless @to_date.between?(from_date, from_date.next_year)
    rescue => e
      puts e.message.red
      retry
    end

    print_date(to_date)
  end

  def count_paid_days
    print "\n\rPlease enter, how many paid holidays you've got (default 0): "
    @paid_holidays = gets.chomp.to_i
    puts "Paid holidays: #{paid_holidays} days or working hours: #{days_to_working_hours(paid_holidays)}"
  end

  def print_date(date)
    puts "Date is set #{date.strftime('%d %b %Y')}\n\r"
  end
end

TogglCounter.call
